//
//  ViewController.swift
//  WeatherApp
//
//  Created by Naveen Jangid on 30/10/23.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var smokeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var txtCity: UITextField!
    
    var viewModel = WeatherViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apiCall(txtCity: "London")
    }
    
    func apiCall(txtCity: String) {
        viewModel.fetchWeather(forCity: txtCity) { [weak self] weatherModel  in
            switch weatherModel {
            case .success(let weather):
                // Update UI with weather details
                DispatchQueue.main.async {
                    self?.updateUI(with: weather)
                }
            case .failure(let error):
                print("Error fetching weather data: \(error.localizedDescription)")
            }
        }
    }
    
    
    func updateUI(with weatherModel: WeatherModel) {
        txtCity.text = weatherModel.name
        if let temperature = weatherModel.main?.temp {
            let temperatureInCelsius = temperature - 273.15
            let roundedTemperature = Int(round(temperatureInCelsius))
            temperatureLabel.text = String("\(roundedTemperature)°")
            print("Current Temperature: \(temperatureInCelsius)°C")
        }
        smokeLabel.text = weatherModel.weather?.first?.main
        
        if let temperatureMax = weatherModel.main?.tempMax , let temperatureMin = weatherModel.main?.tempMin {
            let temperatureInCelsiusMax = temperatureMax - 273.15
            let roundedTemperatureMax = Int(round(temperatureInCelsiusMax))
            
            let temperatureInCelsiusMin = temperatureMin - 273.15
            let roundedTemperatureMin = Int(round(temperatureInCelsiusMin))
            
            descriptionLabel.text = "L:\(roundedTemperatureMin)° H:\(roundedTemperatureMax)°"
        }
        
    }
    
    
    @IBAction func btnSubmit(_ sender: UIButton) {
        
        apiCall(txtCity: txtCity.text ?? "London")
    }
    
    
}
