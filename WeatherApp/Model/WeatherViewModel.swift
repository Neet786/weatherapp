//
//  WeatherViewModel.swift
//  WeatherApp
//
//  Created by Naveen Jangid on 30/10/23.
//

import Foundation

class WeatherViewModel {
    private let apiManager: APIManager
    
    init(apiManager: APIManager = APIManager.shared) {
        self.apiManager = apiManager
    }
    
    func fetchWeather(forCity city: String, completion: @escaping (Result<WeatherModel, Error>) -> Void) {
        apiManager.fetchWeather(forCity: city, completion: completion)
    }
    
}
