//
//  APIManager.swift
//  WeatherApp
//
//  Created by Naveen Jangid on 30/10/23.
//

import Foundation


class APIManager {
    static let shared = APIManager() // Singleton instance

    private let baseURL = "https://api.openweathermap.org/data/2.5/weather"
    private let apiKey = "bec68d65a4fb2d592dc265924cdc951d"

    func fetchWeather(forCity city: String, completion: @escaping (Result<WeatherModel, Error>) -> Void) {
        let urlString = "\(baseURL)?q=\(city)&appid=\(apiKey)"
        
        if let url = URL(string: urlString) {
            URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let error = error {
                    completion(.failure(error))
                    return
                }
                
                if let data = data {
                    do {
                        let weather = try JSONDecoder().decode(WeatherModel.self, from: data)
                        completion(.success(weather))
                    } catch {
                        completion(.failure(error))
                    }
                }
            }.resume()
        }
    }
}

